#CXX = /opt/intel/bin/icpc
CXXFLAGS = -std=c++11 -O0 -g -D__BITS__=128 -I./src/include -MD -MP
#CXXFLAGS = -std=c++11 -O3 -D__BITS__=128 -I./src/include -MD -MP
LIBS = -lmpfr -lreadline
EXEC = $(notdir $(basename $(shell ls $(SRCDIR)/*.cpp)))

OBJDIR = ./.obj
SRCDIR = ./src

all: smbl

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@echo "["$(notdir $(basename $(CXX)))"]" $@
	@$(CXX) $(CXXFLAGS) -o $@ -c $(subst .o,.cpp,$(SRCDIR)/$(notdir $@)) $(LIBS)

%: $(OBJDIR)/%.o
	@mkdir -p $(OBJDIR)
	@echo "[ld]" $@
	@$(CXX) -o $@ $< $(LIBS)

clean:
	@rm -rf $(OBJDIR)
	@rm -f ./smbl

-include $(patsubst %,$(OBJDIR)/%.d,$(EXEC))
