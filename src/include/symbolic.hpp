#ifndef __SYMBOLIC__
#define __SYMBOLIC__
#include <sstream>
#include <limits>
#include <string>
#include <vector>
#include <map>
#include <random>
#include <algorithm>
#include <list>
#include <set>
#include <stdexcept>
#include <real.hpp>
namespace smbl{

	std::default_random_engine gen(1337);

	typedef mpfr::real<__BITS__> real;

	std::string trim(const std::string s){
		std::stringstream r;
		for(size_t i=0; i<s.size(); ++i){
			if(s[i]!=' ' && s[i]!='\n' && s[i]!='\r' && s[i]!='\t') r << s[i];
		}
		return r.str();
	}

	bool isUppercase(const char c){
		return (c>='A' && c<='Z');
	}

	bool isLowercase(const char c){
		return (c>='a' && c<='z');
	}

	bool isNumeric(const char& c){
		return (c>='0' && c<='9');
	}

	real toNumeric(const std::string& s){
		return std::stod(s);
	}

	size_t match(const char& a, const char& b, const std::string& s){
		size_t cnt = 1;
		size_t pos = s.find_first_of(a)+1;
		bool done = false;
		while(!done && pos<s.size()){
			if(s[pos]==b){
				--cnt;
			}
			if(s[pos]==a){
				++cnt;
			}
			if(cnt==0) return pos;
			++pos;
		}
		return std::string::npos;
	}

	const std::string parse_rel_error = "only one type of relation per line allowed";
	const std::string parse_error = "variable name must be [a-zA-Z][0-9]*";
	const std::string parse_dim_error = "cannot parse vector dimension";
	const std::string parse_op_error = "cannot parse operator";
	const std::string parse_op_arg_error = "cannot parse operator argument";
	const std::string symbol_not_found_error = "cannot find symbol";
	const std::string bad_relation = "bad relation";
	const std::string bad_node = "bad node";
	const std::string non_rel_query = "query must be a relation";
	const std::string inconsistent_rel_ops = "operators must be consistent over all relations";
	const std::string cannot_decide = "cannot decide if query relation holds";

	char getRel(const std::string& s, size_t& f){
		f = 0;
		std::list<char> R = {'<','>','=',':'};
		char c;
		for(auto r : R){
			bool found = (s.find(r) != std::string::npos);
			if(found){
				++f;
				c = r;
			}
		}
		if(f>1)
			throw std::invalid_argument(parse_rel_error);
		if(f==0)
			return (char)-1;
		return c;
	}

	class symbol;

	class node{
		public:
			virtual bool isRel() const = 0;
			virtual std::string toString() const = 0;
	};

	class symbol_node : public node{
		private:
			symbol* s = 0;
			std::list<symbol_node*> a;
			char sgn = '+';

		public:
			~symbol_node(){
				for(auto x : a) delete x;
			}

			void negate(){
				if(sgn=='+') sgn = '-';
				else if(sgn=='-') sgn = '+';
			}

			symbol_node(){
			}

			symbol_node(symbol* _s) : node(), s(_s){
			}

			symbol_node(const symbol_node& other) : node(), s(other.symb()){
				for(auto x : other.args()){
					a.push_back(new symbol_node(*x));
				}
			}

			size_t dist(const symbol_node * other) const {
				std::vector<std::string> Pa = paths();
				std::vector<std::string> Pb = other->paths();

				const size_t max_p = std::max(Pa.size(),Pb.size());

				std::set<std::string> S;
				for(auto xa: Pa)
					S.insert(xa);
				for(auto xb: Pb)
					S.insert(xb);

				return S.size()-max_p;
			}

			std::vector<std::string> paths() const;
/*
			symbol_node(symbol* _s) : node(), s(_s) {
				if(_s==0)
					throw std::logic_error(bad_node);
			}

			symbol_node(symbol*& _s, std::list<symbol_node*>& _a) : node(), s(_s), a(_a) {
				if(_s==0)
					throw std::logic_error(bad_node);
			}
*/
			symbol* symb() const {
				return s;
			}

			const std::list<symbol_node*>& args() const { return a; }

			void set_symbol(symbol*& _s){
				s = _s;
			}

			void set_args(std::list<symbol_node*>& _a){
				a = _a;
			}

			bool has_args() const {
				return (a.size()>0);
			}

			virtual std::string toString() const;

			virtual bool isRel() const { return false; }

			bool equals(const symbol_node* other) const;

			size_t vertices() const {
				size_t n=1+a.size();
				for(auto x : a)
					n += x->vertices();
				return n;
			}

			void randomize(double p, char target_rel);
	};

	class relation_node : public node{
		private:
			symbol_node* _lhs = 0;
			symbol_node* _rhs = 0;
			char _R = ' ';

		public:

			relation_node(const char& rel, symbol* l, symbol* r) : node(), _R(rel), _lhs(new symbol_node(l)), _rhs(new symbol_node(r)) {
			}

			~relation_node(){
				if(_lhs) delete _lhs;
				if(_rhs) delete _rhs;
			}

			void set_lhs(symbol_node* l){
				_lhs = l;
			}

			void set_rhs(symbol_node* r){
				_rhs = r;
			}

			void set_rel(const char& r){
				_R = r;
			}

			relation_node() {}

			const symbol_node* lhs() const { return _lhs; }
			const symbol_node* rhs() const { return _rhs; }
			char R() const { return _R; }

			virtual std::string toString() const;

			virtual bool isRel() const{ return true; }

			bool equals(const relation_node* other) const {
				return _R==other->R() && _lhs->equals(other->lhs()) && _rhs->equals(other->rhs());
			}

			bool valid() const;
	};

	class symbol{
		private:
			std::string rep;
			std::set<relation_node*> relations;
			std::set<relation_node*> local_relations;

			int sgn = 0;

			bool cnst_t = false; // expression is constant
			bool set_t  = false; // expression is set
			bool vec_t  = false; // expression is vector
			bool op_t   = false; // expression is operator

			bool vari_t = false; // operator is variadic (associative)
			bool inv_t  = false; // operator is invertible
			bool symm_t = false; // operator is symmetric (commutative)
			bool md_t   = false; // operator is monotonically decreasing
			bool mi_t   = false; // operator is monotonically increasing

			static std::map<std::string,symbol*>* _instances;
			real val;

			symbol* d = 0; // dimension
			symbol* I = 0; // identity/neutral elements

			symbol(const std::string& s, symbol_node* node = 0){
				parse_symbol(s, node);
			}

			static size_t parse_args(std::list<symbol_node*>* argl, std::string target){
				static const std::set<std::pair<std::string,size_t>> special({{"as",1},{"co",2},{"dim",4},{"mi",8},{"md",16},{"inv",512}});
				size_t ret = 0;

				const size_t bpos = target.find_first_of('(');

				if(bpos==std::string::npos) return ret;
				const size_t epos = match('(',')',target);
				if(epos==std::string::npos)
					throw std::invalid_argument(parse_op_error);

				const size_t len = epos-bpos-1;

				target = target.substr(bpos+1,len);

				while(true){
					const size_t c1 = target.find_first_of('(');
					const size_t c2 = target.find_first_of('[');			
					const size_t c3 = target.find_first_of(',');

					bool arg_is_vec = false;
					bool arg_is_op  = false;

					if(c1<c2 && c1<c3){
						arg_is_op  = true;
					}else if(c2<c1 && c2<c3){
						arg_is_vec = true;
					}

					size_t end_pos = c3;

					if(arg_is_vec){
						end_pos = match('[',']',target)+1;
					}else if(arg_is_op){
						end_pos = match('(',')',target)+1;
					}

					std::string arg = target.substr(0,end_pos);
					

					if(arg.size()){
						bool is_special = false;
						size_t rval = 0;

						for(auto p : special){
							if(!arg.compare(p.first)){
								is_special = true;
								ret += p.second;
								break;
							}
						}

						if(!is_special){
							auto x = new symbol_node();
							auto xs = symbol::instance(arg,x);
							x->set_symbol(xs);
							argl->push_back(x);
						}else{	

						}
					}

					if(target.size()==arg.size()) break;
					target = target.substr(end_pos+1);
				}

				return ret;
			}

			void parse_symbol(const std::string& s, symbol_node* node = 0, const bool& known = false, const bool& readonly = false){
				rep = s;

				const size_t b1 = s.find_first_of('(');
				const size_t b2 = s.find_first_of('[');
				size_t bpos = s.size();

				if(b1!=std::string::npos && b2!=std::string::npos && b1<b2){
					bpos = b1;
					op_t = true;
				}else if(b1!=std::string::npos && b2!=std::string::npos && b2<b1){
					bpos = b2;
					vec_t = true;
				}else if(b1!=std::string::npos && b2==std::string::npos){
					bpos = b1;
					op_t = true;
				}else if(b1==std::string::npos && b2!=std::string::npos){
					bpos = b2;
					vec_t = true;
				}

				if(vec_t){
					rep = s.substr(0,bpos);
					const size_t epos = match('[',']',s);

					if(epos==std::string::npos && !known)
						throw std::invalid_argument(parse_dim_error);
					else if (epos==std::string::npos && known)
						return;

					const size_t len = epos-bpos-1;
					if(len==0)
						d = symbol::instance("inf");
					else
						d = symbol::instance(s.substr(bpos+1,len));

				}else if(op_t){
					rep = s.substr(0,bpos);
					std::list<symbol_node*> args;

					const size_t r = parse_args(&args,s);

					if(node!=0){
						if(!(node->isRel())){
							symbol_node* sym_root = (symbol_node*)node;
							node->set_args(args);
						}
					}

					if(readonly) return;

					if(r&1) vari_t = true;
					if(r&2) symm_t = true;
					if(r&8) mi_t = true;
					if(r&16) md_t = true;
					if(r&512) inv_t = true;

					if(!vari_t)
						d = symbol::instance(std::to_string(args.size()));
					else
						d = symbol::instance("inf");
				}else{
					// only if not vec and not op
				}

				if(known) return;

				//addLower(this);
				//addUpper(this);
				//addEquiv(this);

				try{
					if(!s.compare("Pi"))
						val = 3.14159265359;
					else
						val = toNumeric(s);

					this->cnst_t = true;

					if(val==0) this->sgn =  0;
					if(val<0)  this->sgn = -1;
					if(val>0)  this->sgn = +1;

				}catch(...){
					if(!vec_t){
						addUpper(symbol::instance("inf"));
						addLower(symbol::instance("-inf"));
					}

					sgn = 0;

					if(rep[0]=='-'){
						rep = rep.substr(1,rep.size()-1);
						sgn = -1;
						addUpper(symbol::instance("0"));
					}

					if(rep[0]=='+'){
						rep = rep.substr(1,rep.size()-1);
						sgn = +1;
						addLower(symbol::instance("0"));
					}

					char c = s[0];

					if(!(isLowercase(c) || isUppercase(c))){
						throw std::invalid_argument(parse_error);
					}

					size_t i=1;
					while( isLowercase(s[i]) || isUppercase(s[i]) ) ++i;

					for(; i<bpos; ++i){
						c = s[i];
						if(!(isNumeric(c))){
							throw std::invalid_argument(parse_error);
						}
					}
				}
			}

			void addLocalRelation(relation_node*& o){
				for(auto r : local_relations) if(r->equals(o)){ delete o; return;}
				local_relations.insert(o);
			}

		public:
			~symbol(){
				for(auto r : local_relations){
					if(r->lhs()->symb()==this){
						delete r;
					}
				}
			}

			virtual std::string name() const {
				return rep;
			}

			std::set<relation_node*> const & rels() const {
				return relations;
			}

			static void clear(){
				if( _instances->size()>0 ){
					for(auto _i : (*_instances))
						delete _i.second;
				}
				_instances->clear();
			}

			static double dist(const relation_node * a, const relation_node * b){
				if(a->R()!=b->R())
					return std::stod("inf");

				return a->lhs()->dist(b->lhs()) + a->rhs()->dist(b->rhs());
			}

			static bool infer(const relation_node * q){
				const symbol_node* s = q->lhs();
				const symbol_node* t = q->rhs();

				const symbol* l = s->symb();
				const symbol* r = t->symb();
				
				if(l->constant() && r->constant() && q->R() == '<')
					return l->value() <= r->value();
				else if(l->constant() && r->constant() && q->R() == '>')
					return l->value() >= r->value();
				else if(l->constant() && r->constant() && q->R() == '=')
					return l->value() == r->value();

				symbol_node current(*s);

				const size_t k = 4;

				for(size_t i=0; i<16; ++i){
					// generate k random candidates
					std::vector<symbol_node*> C;
					
					for(size_t j=0; j<k; ++j){
						// copy current
						symbol_node* c = new symbol_node(current);
						c->randomize(0.5,q->R());
						C.push_back(c);
					}

					size_t score_id = 0;
					size_t score = t->dist(C[score_id]);

					for(size_t j=1; j<C.size(); ++j){
						size_t cs = t->dist(C[j]);
						if(cs<score){
							score_id = j;
							score = cs;
						}
					}

					std::cout << "LOG: " << s->symb()->name() << " " << q->R() << " " << C[score_id]->symb()->name() << std::endl;

					if(score>0 && C[score_id]->symb()->constant() && t->symb()->constant() && q->R() == '='){
						return C[score_id]->symb()->value() == t->symb()->value();
					}else if(score>0 && C[score_id]->symb()->constant() && t->symb()->constant() && q->R() == '<'){
						return C[score_id]->symb()->value() <= t->symb()->value();
					}else if(score>0 && C[score_id]->symb()->constant() && t->symb()->constant() && q->R() == '>'){
						return C[score_id]->symb()->value() >= t->symb()->value();
					}else if(score==0){
						for(auto c : C) delete c;
						return true;
					}else{
						current = *(C[score_id]);
						for(auto c : C) delete c;
					}
				}
					
				throw std::logic_error(cannot_decide);
			}

			static relation_node* load_query(const std::string& _s){
				return (relation_node*)load_fact(_s,true);
			}

			static node* load_fact(const std::string& _s, bool readonly = false){
				node* root = 0;

				size_t rc = 0;
				char c = smbl::getRel(_s,rc);
				if(rc>1)
					throw std::invalid_argument(non_rel_query);

				if(c==(char)-1){
					root = new symbol_node();
				}else
					root = new relation_node();

				if(root->isRel()){
					relation_node* rel_root = (relation_node*)root;

					rel_root->set_rel(c);

					std::string s;

					std::stringstream tS(_s);

					std::getline(tS,s,c);
					auto x = new symbol_node();
					auto xs = smbl::symbol::instance(s,x,readonly);
					x->set_symbol(xs);

					std::getline(tS,s,c);
					auto y = new symbol_node();
					auto ys = smbl::symbol::instance(s,y,readonly);
					y->set_symbol(ys);

					rel_root->set_lhs(x);
					rel_root->set_rhs(y);

					if(!readonly && c!=':'){
						xs->addRelation(rel_root);
						ys->addRelation(rel_root);
					}
				}else{
					if(readonly){
						delete (symbol_node*) root;
						throw std::invalid_argument(non_rel_query);
					}
					symbol_node* sym_root = (symbol_node*)root;

					auto xs = smbl::symbol::instance(_s,sym_root,readonly);
					sym_root->set_symbol(xs);
				}

				return root;
			}

			static symbol* instance(const std::string& _s, symbol_node* node = 0, bool readonly = false){
				const std::string s = trim(_s);

				static CGuard g;
				size_t len = s.size();

				for(size_t i=0; i<len; ++i){
					if(!(isLowercase(s[i]) || isUppercase(s[i]) || isNumeric(s[i])) ){
						if(s[i]=='[' || s[i]=='(')
							len=i;
						break;
					}
				}

				std::string key = s.substr(0,len);

				bool getDim = false;
				bool getSgn = false;

				if(!key.substr(0,3).compare("dim")){
					getDim = true;
					key = s.substr(4,s.size()-5);
				}

				if(!key.substr(0,3).compare("sgn")){
					getSgn = true;
					key = s.substr(4,s.size()-5);
				}

				if(_instances->find(key)==_instances->end()){
					if(getDim)
						throw std::invalid_argument(symbol_not_found_error);
					auto S = new symbol(s,node);

					if(S->negative() && !(S->constant())){
						key = key.substr(1,len-1);
					}

					(*_instances)[key] = S;

				}else if(getDim){
					return (*_instances)[key]->dim();
				}else if(getSgn){
					return (*_instances)[key]->sign();
				}else{
					auto S = (*_instances)[key];
					S->parse_symbol(s,node,true,readonly);
				}

				return (*_instances)[key];
			}

			static symbol* instance(const int& n){
				std::stringstream sn;
				sn << n;
				return instance(sn.str());
			}

			static symbol* instance(const size_t& n){
				std::stringstream sn;
				sn << n;
				return instance(sn.str());
			}

			static symbol* instance(const long& n){
				std::stringstream sn;
				sn << n;
				return instance(sn.str());
			}

			static symbol* instance(const double& n){
				std::stringstream sn;
				sn << n;
				return instance(sn.str());
			}

			static std::map<std::string,symbol*>* table(){
				return _instances;
			}
/*
			symbol* at(symbol* idx){
				if(!vector()) return 0;
				return V[idx];
			}
*/
			class CGuard{
				public:
					~CGuard(){
						clear();
						delete _instances;
						_instances = 0;
					}
			};

			real value() const {
				if(constant())
					return sgn*val;
				return 0;
			}

			symbol* dim() const { return d; }

			std::string toString(const bool& ll = false) const {
				std::stringstream os;
				os << "[" << this << ' ' << this->name();
//				if(!ll){
//					os << "]";
//					return os.str();
//				}
				if(ll && local_relations.size() + relations.size() > 0){
					os << " RELS = [";
					bool first = true;
					for(auto r : local_relations){
						if(first) first=false;
						else os << ',';
						os << r->toString();
					}
					for(auto r : relations){
						if(first) first=false;
						else os << ',';
						os << r->toString();
					}
					os << "]";
				}
				if(d){
					os << " DIM = " << d; 
				}
				if(positive()){
					os << " POSITIVE";
				}
				if(negative()){
					os << " NEGATIVE";
				}
				if(constant()){
					os << " CONSTANT";
				}
				if(vector()){
					os << " VECTOR";
				}
				if(set()){
					os << " SET";
				}
				if(op()){
					os << " OPERATOR";
				}
				if(associative()){
					os << " ASSOCIATIVE";
				}
				if(commutative()){
					os << " COMMUTATIVE";
				}
				if(invertible()){
					os << " INVERTIBLE";
				}
				if(mincreasing()){
					os << " MONOTONICALLY_INCREASING";
				}
				if(mdecreasing()){
					os << " MONOTONICALLY_DECREASING";
				}
				if(I){
					os << " IDENTITY = " << I;
				}
				os << "]";
				return os.str();
			}

			virtual symbol* sign() const {
				if(sgn>0) return symbol::instance("1");
				if(sgn<0) return symbol::instance("-1");
				return symbol::instance("0");
			}
			virtual void setSign(const int& s) { 
				sgn=s;
				//return sign();
			}

			bool positive() const { return sgn>0; }
			bool negative() const { return sgn<0; }

			virtual bool valid() const {
				return true;
			}

			void addRelation(relation_node*& o){
				for(auto r : relations) if(r->equals(o)){ delete o; return;}
				relations.insert(o);
				if(!consistent())
					throw std::invalid_argument(inconsistent_rel_ops);
			}

			void delRelation(relation_node*& o){
				for(auto r : relations) if(r->equals(o)){ delete r; return;}
			}

			void addUpper(symbol* s){
				auto r = new relation_node('<',this,s);
				addLocalRelation(r);
				s->addRelation(r);
			}

			void addLower(symbol* s){
				auto r = new relation_node('>',this,s);
				addLocalRelation(r);
				s->addRelation(r);
			}

			void addEquiv(symbol* s){
				auto r = new relation_node('=',this,s);
				addLocalRelation(r);
				s->addRelation(r);
			}

			bool isEquiv(symbol* s) const {
				if(s==this) return true;
				for(auto r : relations){
					if(r->R()=='=')
						if((r->lhs()->symb()==this && r->rhs()->symb()==s) || (r->rhs()->symb()==this && r->lhs()->symb()==s))
							return true;
				}
				for(auto r : local_relations){
					if(r->R()=='=')
						if(	(r->lhs()->symb()==this && r->rhs()->symb()==s) ||
							(r->rhs()->symb()==this && r->lhs()->symb()==s) )
							return true;
				}
				return false;
			}


			bool consistent(const relation_node* target = 0) const {
				//std::cout << "CHECK CONSISTENCY FOR " << this->name() << " " << d << std::endl;
				if(!op() || (op() && (commutative()||associative()))) return true;
				std::vector<bool>* f = 0;
				if(target){
					f = new std::vector<bool>();
					symbol_node const * v = target->lhs();
					if(v->symb()!=this)
						v = target->rhs();
					for(auto w : v->args())
						f->push_back(w->symb()->vector());
				}
				for(auto r : relations){
					if(f==0){
						f = new std::vector<bool>();
						symbol_node const * v = r->lhs();
						if(v->symb()!=this)
							v = r->rhs();
						for(auto w : v->args())
							f->push_back(w->symb()->vector());
					}else{
						symbol_node const * v = r->lhs();
						if(v->symb()!=this)
							v = r->rhs();
						size_t i = 0;

						if(v->args().size() != f->size()) // wrong num of args
							return false;

						for(auto w : v->args()){
							try{
								if(f->at(i++)!=w->symb()->vector())
									return false;
							}catch(...){
								
								return false;
							}
						}
					}
				}
				if(f) delete f;
				return true;
			}

			void setIdent(symbol* _i){
				I = _i;
			}

			size_t constant() const { return cnst_t?256:0; }
			size_t invertible() const { return inv_t?512:0; }
			size_t set() const { return set_t?128:0; }
			size_t vector() const { return vec_t?64:0; }
			size_t op() const { return op_t?32:0; }
			size_t associative() const { return vari_t?1:0; }
			size_t commutative() const { return symm_t?2:0; }
			size_t mincreasing() const { return mi_t?8:0; }
			size_t mdecreasing() const { return md_t?16:0; }

			size_t properties() const {
				return constant() + set() + vector() + op() + associative() + commutative() + mincreasing() + mdecreasing() + invertible();
			}

			bool same_type(const symbol*& other) const {
				return (this->properties() == other->properties());
			}
	};
	std::map<std::string,symbol*>* symbol::_instances = new std::map<std::string,symbol*>();

	std::string symbol_node::toString() const {
		std::stringstream os;
		os << sgn << s->name();
		if(s->op()){
			os << "(";
			bool first = true;
			for(auto v : a){
				if(first) first = false;
				else os << ',';
				os << v->toString();
			}
			if(first) os << '*';
			os << ")";
		}	
		return os.str();
	}

	std::string relation_node::toString() const {
		std::stringstream os;
		os << _lhs->toString() << ' ' << _R << ' ' << _rhs->toString();
		return os.str();
	}

	bool symbol_node::equals(const symbol_node* other) const {
		if(s!=other->symb()) return false;
		return this->dist(other)==0;
	}

	void symbol_node::randomize(double p, char target_rel){
		std::bernoulli_distribution db(p);
		if(db(gen) || a.size()==0){ // TODO: correct the stopping prob!
			// stop at this vertex
			// choose rel

			if(symb()->rels().size()==0) return;

			std::uniform_int_distribution<size_t> dr(0,symb()->rels().size()-1);
			const size_t rc = dr(gen);

			auto r = symb()->rels().begin();
			for(size_t i=0; i<rc; ++i) r++;

			relation_node* rel = *r;

			for(auto x : a)
				delete x;
			a.clear();

			// std::cout << "WOULD APPLY " << rel->toString() << std::endl;
			// apply relation
			if(rel->R()=='='){
				if(rel->lhs()->symb()==symb()){
					s = rel->rhs()->symb();
					for(auto y : rel->rhs()->args())
						a.push_back(new symbol_node(*y));
				}else{
					s = rel->lhs()->symb();
					for(auto y : rel->lhs()->args())
						a.push_back(new symbol_node(*y));
				}

			}else if (rel->R()=='<'){
				if(rel->lhs()->symb()==symb()){
					if(target_rel=='<'){
						s = rel->rhs()->symb();
						for(auto y : rel->rhs()->args())
							a.push_back(new symbol_node(*y));
					}
				}else{
					if(target_rel=='>'){
						s = rel->lhs()->symb();
						for(auto y : rel->lhs()->args())
							a.push_back(new symbol_node(*y));
					}
				}

			}else if (rel->R()=='>'){
				if(rel->lhs()->symb()==symb()){
					if(target_rel=='>'){
						s = rel->rhs()->symb();
						for(auto y : rel->rhs()->args())
							a.push_back(new symbol_node(*y));
					}
				}else{
					if(target_rel=='<'){
						s = rel->lhs()->symb();
						for(auto y : rel->lhs()->args())
							a.push_back(new symbol_node(*y));
					}
				}
			}

			return;
			
			throw std::logic_error(bad_relation);
		}else{
			// choose child
			std::uniform_int_distribution<size_t> dm(0,a.size()-1);
			const size_t child = dm(gen);

			auto c = a.begin();
			for(size_t i=0; i<child; ++i) c++;

			(*c)->randomize(p,target_rel);
		}
	}

	std::vector<std::string> symbol_node::paths() const {
		std::vector<std::string> P;

		if(a.size())
			for(auto v : a){
				std::vector<std::string> Pv = v->paths();

				for(auto x : Pv){
					P.push_back(s->name() + '.' + x);
				}
			}
		else
			P.push_back(s->name());

		if(symb()->op() && symb()->commutative()){
			std::sort(begin(P),end(P));
		}

		return P;
	}

	bool relation_node::valid() const {
		const bool type_invalid = (lhs()->symb()->vector() && !rhs()->symb()->vector()) || (!lhs()->symb()->vector() && rhs()->symb()->vector());
		const bool r_invalid = lhs()->symb()->vector() && rhs()->symb()->vector() && (R()=='>' || R()=='<');
		//std::cout << vdim_invalid << ' ' << type_invalid << std::endl;
		const bool rel_inconsistent = !(lhs()->symb()->consistent(this)) || !(rhs()->symb()->consistent(this));
		return !r_invalid && !type_invalid && !rel_inconsistent;
	}

	static std::list<smbl::node*> load_file(const std::string& fn){
		std::list<smbl::node*> T;
		std::ifstream inF(fn);
		size_t ln = 1;
		while(!inF.eof()){
			std::string line, tline;
			std::getline(inF,line);
			tline = smbl::trim(line);
			if(tline.size() && tline[0]!='#'){
				smbl::node* t = smbl::symbol::load_fact(tline);
				T.push_back(t);
			}
		}
		inF.close();
		return T;
	}
};
#endif
