#include <iostream>
#include <fstream>
#include <string>

#include <readline/readline.h>
#include <readline/history.h>

#include <symbolic.hpp>

int main(int argc, char** argv){
	if(argc!=2){
		std::cout << "usage: " << argv[0] << " <file>" << std::endl;
		return 128;
	}

	std::cout << "SYMBOLICA; Copyright (c) 2018 Nico Piatkowski" << std::endl;

	std::list<smbl::node*> T = smbl::load_file(argv[1]);

	char *buf;
	while((buf = readline("> "))!=NULL){
		std::string cmd(buf);

		if(!cmd.compare("exit") || !cmd.compare("q"))
			break;

		if(!cmd.compare("tbl")){
			for(auto s : *(smbl::symbol::table())){
				std::cout << s.second->toString() << std::endl;
			}
			add_history(buf);
			continue;
		}

		if(!cmd.compare("ltbl")){
			for(auto s : *(smbl::symbol::table())){
				std::cout << s.second->toString(true) << std::endl;
			}
			add_history(buf);
			continue;
		}

		smbl::relation_node* q = 0;

		if(cmd.size()){
			try{
				q = smbl::symbol::load_query(cmd);

				if(!q->valid()){
					delete q;
					std::cout << "INVALID" << std::endl;
					continue;
				}

				add_history(buf);
//				std::cout << "Q: " << q->lhs()->toString() << ' ' << q->R() << ' ' << q->rhs()->toString() << std::endl;

				const bool result = smbl::symbol::infer(q);

				if(result)
					std::cout << "TRUE" << std::endl;
				else
					std::cout << "FALSE" << std::endl;

			}catch(...){
				std::cout << "UNKNOWN" << std::endl;
			}

			if(q) delete (smbl::relation_node*)q;
		}

		free(buf);
	}

	for(auto t : T){
		if(t->isRel())
			delete (smbl::relation_node*)t;
		else
			delete (smbl::symbol_node*)t;
	}
	
	return 0;
}
